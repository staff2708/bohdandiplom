import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { HeroCreationComponent } from './components/hero-creation/hero-creation.component';
import { AllHeroesComponent } from './components/all-heroes/all-heroes.component';
import { MainPageComponent } from './components/main-page/main-page.component';

const appRoutes: Routes = [
  { path: '', component: MainPageComponent },
  { path: 'create', component: HeroCreationComponent },
  { path: 'all', component: AllHeroesComponent},
  // @ts-ignore
  { path: 'auth', loadChildren: () => import ('./modules/auth/auth.module').then(mod => mod.AuthModule)}
];


@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [RouterModule]

})

export class AppRouting { }
