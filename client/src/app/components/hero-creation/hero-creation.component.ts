import { Component, Input, OnInit } from '@angular/core';
import { mainCharacteristcs, skills, secondaryCharacteristics } from '../../data/characteristics';
import {
  IArmor,
  ICharacter,
  ICharacteristic,
  ISkill,
  ITalent,
  ITrapings,
  IWeapon, MainCharacteristics
} from '../../interfaces/character.interface';
import { FormArray, FormControl, FormGroup } from '@angular/forms';
import { HttpCharacterService } from '../../services/http-character.service';


@Component({
  selector: 'app-hero-creation',
  templateUrl: './hero-creation.component.html',
  styleUrls: ['./hero-creation.component.scss']
})
export class HeroCreationComponent implements OnInit {
  public isPersonalInfoVisible = true;
  public isCharacterProfileVisible = true;
  public isTalentsVisible = true;
  public isCharacterVisible = true;
  public baseSkills: ISkill[] = [...skills];
  public baseTalents: ITalent[] = [{
    name: '',
    description: '',
  }];
  @Input() character: ICharacter;

  public baseMainCharacteristcs: ICharacteristic[] = [...mainCharacteristcs];
  public baseSecondaryCharacteristics: ICharacteristic[] = [...secondaryCharacteristics];

  public form = new FormGroup({
    name: new FormControl(''),
    race: new FormControl(''),
    careerPath: new FormControl(''),
    totalExp: new FormControl(0),
    spendExp: new FormControl(0),
    gender: new FormControl(''),
    birthDate: new FormControl(''),
    nationality: new FormControl(''),
    height: new FormControl(''),
    eyes: new FormControl(''),
    age: new FormControl(''),
    birthplace: new FormControl(''),
    religion: new FormControl(''),
    weight: new FormControl(''),
    hair: new FormControl(''),
    distinguishingMark: new FormControl(''),
    maxEnc: new FormControl(0),
    totalEnc: new FormControl(0),
    mainCharacteristics: new FormArray([]),
    secondaryCharacteristics: new FormArray([]),
    talents: new FormArray([]),
    skills: new FormArray([]),
    inventory: new FormGroup({
      trappings: new FormArray([
        new FormGroup({
          item: new FormControl(''),
          location: new FormControl(''),
          enc: new FormControl(0),
        })
      ]),
      weapons: new FormArray([
        new FormGroup({
          name: new FormControl(''),
          group: new FormControl(''),
          damage: new FormControl(''),
          range: new FormControl(''),
          reload: new FormControl(''),
          qualities: new FormControl(''),
        })
      ]),
      armor: new FormArray([
        new FormGroup({
          armorType: new FormControl(''),
          locationCovered: new FormControl(''),
          Enc: new FormControl(0),
          AP: new FormControl(0),
        })
      ]),
      moneyAndTreasure: new FormGroup({
        gold: new FormControl(0),
        silver: new FormControl(0),
        brass: new FormControl(0),
        treasures: new FormControl('')
      }),
    }),
    movement: new FormGroup({
      M: new FormControl(0),
      armouredM: new FormControl(0),
      move: new FormControl(0),
      charge: new FormControl(0),
      run: new FormControl(0),
      runningLeap: new FormControl(0),
      standingLeap: new FormControl(0),
      flying: new FormControl(0),
      hamperedMovement: new FormGroup({
        yardsPerMinute: new FormControl(0),
        milesPerMinute: new FormControl(0),
      }),
      standartMovement: new FormGroup({
        yardsPerMinute: new FormControl(0),
        milesPerMinute: new FormControl(0),
      }),
    }),
    spells: new FormArray([]),
  });

  constructor(
    private http: HttpCharacterService
  ) {
  }

  get skills() {
    return this.form.get('skills') as FormArray;
  }

  get mainCharacteristics() {
    return this.form.get('mainCharacteristics') as FormArray;
  }

  get secondaryCharacteristics() {
    return this.form.get('secondaryCharacteristics') as FormArray;
  }

  get talents() {
    return this.form.get('talents') as FormArray;
  }

  get inventory() {
    return this.form.get('inventory') as FormGroup;
  }

  get trappings() {
    return this.inventory.get('trappings') as FormArray;
  }

  get armor() {
    return this.inventory.get('armor') as FormArray;
  }

  get weapons() {
    return this.inventory.get('weapons') as FormArray;
  }

  ngOnInit() {
    if ( !this.character ) {
      this.baseTalents.forEach(( talent: ITalent ) => {
        this.talents.push(new FormGroup({
          name: new FormControl(talent.name),
          description: new FormControl(talent.description)
        }));
      });

      this.baseMainCharacteristcs.forEach(( characteristic: ICharacteristic ) => {
        this.mainCharacteristics.push(new FormGroup({
          type: new FormControl(characteristic.type),
          name: new FormControl(characteristic.name),
          starting: new FormControl(characteristic.starting),
          advanced: new FormControl(characteristic.advanced),
          current: new FormControl(characteristic.current),
        }));
      });
      this.baseSecondaryCharacteristics.forEach(( characteristic: ICharacteristic ) => {
        this.secondaryCharacteristics.push(new FormGroup({
          type: new FormControl(characteristic.type),
          name: new FormControl(characteristic.name),
          starting: new FormControl(characteristic.starting),
          advanced: new FormControl(characteristic.advanced),
          current: new FormControl(characteristic.current),
        }));
      });
      this.baseSkills.forEach(( skill: ISkill ) => {
        this.skills.push(new FormGroup({
          name: new FormControl(skill.name),
          taken: new FormControl(skill.taken),
          total: new FormControl(skill.total),
          relatedCharacteristic: new FormControl(skill.relatedCharacteristic),
          relatedTalent: new FormControl(skill.relatedTalent),
        }));
      });
    }
  }

  public togglePersonalInfo(): void {
    this.isPersonalInfoVisible = !this.isPersonalInfoVisible;
  }

  public toggleCharacterProfile(): void {
    this.isCharacterProfileVisible = !this.isCharacterProfileVisible;
  }

  public toggleTalents(): void {
    this.isTalentsVisible = !this.isTalentsVisible;
  }

  public toggleCharacter(): void {
    this.isCharacterVisible = !this.isCharacterVisible;
  }

  public addNewArmor(): void {
    this.armor.push(
      new FormGroup({
        armorType: new FormControl(''),
        locationCovered: new FormControl(''),
        Enc: new FormControl(0),
        AP: new FormControl(0),
      }))
  }

  public addNewWeapon(): void {
    this.weapons.push(
      new FormGroup({
        name: new FormControl(''),
        group: new FormControl(''),
        damage: new FormControl(''),
        range: new FormControl(''),
        reload: new FormControl(''),
        qualities: new FormControl(''),
      })
    )
  }

  public addNewTrappings(): void {
    this.trappings.push(new FormGroup({
      item: new FormControl(''),
      location: new FormControl(''),
      enc: new FormControl(0),
    }))
  }

  public addNewTalent(): void {
    this.talents.push(
      new FormGroup({
        name: new FormControl(''),
        description: new FormControl(''),
      }))
  }

  public addNewSkill(): void {
    this.skills.push(
      new FormGroup({
        name: new FormControl(''),
        taken: new FormControl(false),
        total: new FormControl(0),
        relatedCharacteristic: new FormControl(''),
        relatedTalent: new FormControl(''),
      }))
  }

  public onSubmit(): void {
    this.http.createHero(this.form.value).subscribe()
  }

}
