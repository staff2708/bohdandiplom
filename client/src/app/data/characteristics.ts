import { ICharacteristic, ISkill } from '../interfaces/character.interface';

export const skills: ISkill[] = [
  {
    name: 'Animal Care',
    taken: false,
    total: 0,
    relatedCharacteristic: 'Int',
    relatedTalent: ''
  },
  {
    name: 'Charm',
    taken: false,
    total: 0,
    relatedCharacteristic: 'Fel',
    relatedTalent: ''
  },
  {
    name: 'Command',
    taken: false,
    total: 0,
    relatedCharacteristic: 'Fel',
    relatedTalent: ''
  },
  {
    name: 'Concealment',
    taken: false,
    total: 0,
    relatedCharacteristic: 'Ag',
    relatedTalent: ''
  },
  {
    name: 'Consume Alcohol',
    taken: false,
    total: 0,
    relatedCharacteristic: 'T',
    relatedTalent: ''
  },
  {
    name: 'Disguise',
    taken: false,
    total: 0,
    relatedCharacteristic: 'Fel',
    relatedTalent: ''
  },
  {
    name: 'Drive',
    taken: false,
    total: 0,
    relatedCharacteristic: 'S',
    relatedTalent: ''
  },
  {
    name: 'Evaluate',
    taken: false,
    total: 0,
    relatedCharacteristic: 'Int',
    relatedTalent: ''
  },
  {
    name: 'Gamble',
    taken: false,
    total: 0,
    relatedCharacteristic: 'Int',
    relatedTalent: ''
  },
  {
    name: 'Gossip',
    taken: false,
    total: 0,
    relatedCharacteristic: 'Fel',
    relatedTalent: ''
  },
  {
    name: 'Haggle',
    taken: false,
    total: 0,
    relatedCharacteristic: 'Fel',
    relatedTalent: ''
  },
  {
    name: 'Intimidate',
    taken: false,
    total: 0,
    relatedCharacteristic: 'S',
    relatedTalent: ''
  },
  {
    name: 'Outdoor Survival',
    taken: false,
    total: 0,
    relatedCharacteristic: 'Int',
    relatedTalent: ''
  },
  {
    name: 'Perception',
    taken: false,
    total: 0,
    relatedCharacteristic: 'Int',
    relatedTalent: ''
  },
  {
    name: 'Ride',
    taken: false,
    total: 0,
    relatedCharacteristic: 'Ag',
    relatedTalent: ''
  },
  {
    name: 'Row',
    taken: false,
    total: 0,
    relatedCharacteristic: 'S',
    relatedTalent: ''
  },
  {
    name: 'Scale Sheer Surface',
    taken: false,
    total: 0,
    relatedCharacteristic: 'S',
    relatedTalent: ''
  },
  {
    name: 'Search',
    taken: false,
    total: 0,
    relatedCharacteristic: 'Int',
    relatedTalent: ''
  },
  {
    name: 'Silent Move',
    taken: false,
    total: 0,
    relatedCharacteristic: 'Ag',
    relatedTalent: ''
  },
  {
    name: 'Swim',
    taken: false,
    total: 0,
    relatedCharacteristic: 'S',
    relatedTalent: ''
  },
  {
    name: 'Animal Training',
    taken: false,
    total: 0,
    relatedCharacteristic: 'Fel',
    relatedTalent: ''
  },
  {
    name: 'Blather',
    taken: false,
    total: 0,
    relatedCharacteristic: 'Fel',
    relatedTalent: ''
  },
  {
    name: 'Chanelling',
    taken: false,
    total: 0,
    relatedCharacteristic: 'WP',
    relatedTalent: ''
  },
  {
    name: 'Charm Animal',
    taken: false,
    total: 0,
    relatedCharacteristic: 'Fel',
    relatedTalent: ''
  },
  {
    name: 'Dodge Blow',
    taken: false,
    total: 0,
    relatedCharacteristic: 'Ag',
    relatedTalent: ''
  },
  {
    name: 'Follow Trail',
    taken: false,
    total: 0,
    relatedCharacteristic: 'Int',
    relatedTalent: ''
  },
  {
    name: 'Heal',
    taken: false,
    total: 0,
    relatedCharacteristic: 'Int',
    relatedTalent: ''
  },
  {
    name: 'Hypnotism',
    taken: false,
    total: 0,
    relatedCharacteristic: 'WP',
    relatedTalent: ''
  },
  {
    name: 'Lip Reading',
    taken: false,
    total: 0,
    relatedCharacteristic: 'Int',
    relatedTalent: ''
  },
  {
    name: 'Magical Sense',
    taken: false,
    total: 0,
    relatedCharacteristic: 'WP',
    relatedTalent: ''
  },
  {
    name: 'Navigation',
    taken: false,
    total: 0,
    relatedCharacteristic: 'Int',
    relatedTalent: ''
  },
  {
    name: 'Pick Lock',
    taken: false,
    total: 0,
    relatedCharacteristic: 'Ag',
    relatedTalent: ''
  },
  {
    name: 'Prepare Poison',
    taken: false,
    total: 0,
    relatedCharacteristic: 'Int',
    relatedTalent: ''
  },
  {
    name: 'Read/Write',
    taken: false,
    total: 0,
    relatedCharacteristic: 'Int',
    relatedTalent: ''
  },
  {
    name: 'Sail',
    taken: false,
    total: 0,
    relatedCharacteristic: 'Ag',
    relatedTalent: ''
  },
  {
    name: 'Set Trap',
    taken: false,
    total: 0,
    relatedCharacteristic: 'Ag',
    relatedTalent: ''
  },
  {
    name: 'Shadowing',
    taken: false,
    total: 0,
    relatedCharacteristic: 'Ag',
    relatedTalent: ''
  },
  {
    name: 'Sleight of Hand',
    taken: false,
    total: 0,
    relatedCharacteristic: 'Ag',
    relatedTalent: ''
  },
  {
    name: 'Torture',
    taken: false,
    total: 0,
    relatedCharacteristic: 'Fel',
    relatedTalent: ''
  },
  {
    name: 'Ventriloquism',
    taken: false,
    total: 0,
    relatedCharacteristic: 'Int',
    relatedTalent: ''
  }
];

export const mainCharacteristcs: ICharacteristic[] = [
  {
    type: 'main',
    name: 'WS',
    starting: 0,
    advanced: 0,
    current: 0
  },
  {
    type: 'main',
    name: 'BS',
    starting: 0,
    advanced: 0,
    current: 0
  },
  {
    type: 'main',
    name: 'S',
    starting: 0,
    advanced: 0,
    current: 0
  },
  {
    type: 'main',
    name: 'T',
    starting: 0,
    advanced: 0,
    current: 0
  },
  {
    type: 'main',
    name: 'Ag',
    starting: 0,
    advanced: 0,
    current: 0
  },
  {
    type: 'main',
    name: 'Int',
    starting: 0,
    advanced: 0,
    current: 0
  },
  {
    type: 'main',
    name: 'WP',
    starting: 0,
    advanced: 0,
    current: 0
  },
  {
    type: 'main',
    name: 'Fel',
    starting: 0,
    advanced: 0,
    current: 0
  },
];

export const secondaryCharacteristics: ICharacteristic[] = [
  {
    type: 'secondary',
    name: 'A',
    starting: 0,
    advanced: 0,
    current: 0
  },
  {
    type: 'secondary',
    name: 'W',
    starting: 0,
    advanced: 0,
    current: 0
  },
  {
    type: 'secondary',
    name: 'SB',
    starting: 0,
    advanced: 0,
    current: 0
  },
  {
    type: 'secondary',
    name: 'TB',
    starting: 0,
    advanced: 0,
    current: 0
  },
  {
    type: 'secondary',
    name: 'M',
    starting: 0,
    advanced: 0,
    current: 0
  },
  {
    type: 'secondary',
    name: 'Mag',
    starting: 0,
    advanced: 0,
    current: 0
  },
  {
    type: 'secondary',
    name: 'IP',
    starting: 0,
    advanced: 0,
    current: 0
  },
  {
    type: 'secondary',
    name: 'FP',
    starting: 0,
    advanced: 0,
    current: 0
  },
];
