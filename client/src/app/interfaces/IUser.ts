import { ICharacter } from './character.interface';

export interface IUser {
  email: string,
  nickname: string,
  password?: string,
  token?: string,
  characters?: ICharacter[],
  groups?: any[],
  image?: any
}
