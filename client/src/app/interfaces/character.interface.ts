export interface ITrapings {
  item: string;
  location: string;
  Enc: number;
}

export interface IWeapon {
  name: string;
  group: string;
  damage: string;
  range: string;
  reload: string;
  qualities: string;
}

export interface IArmor {
  armorType: string;
  locationCovered: string;
  Enc: number;
  AP: number;
}

export interface IGoldAndTreasure {
  gold: number;
  silver: number;
  brass: number;
  treasures: string[];
}

export interface IMovement {
  M: number;
  armouredM: number;
  move: number;
  charge: number;
  run: number;
  runningLeap: number;
  standingLeap: number;
  flying: number;
  hamperedMovement: IMovementAbstraction;
  standartMovement: IMovementAbstraction;
}

export interface ISpell {
  name: string;
  castingNumber: number;
  castingTime: string;
  ingredients: string;
  descriptions: string;
}

export interface IMovementAbstraction {
  yardsPerMinute: number;
  milesPerMinute: number;
}

export interface ITalent {
  name: string;
  description: string;
}

export type MainCharacteristics = 'WS' | 'BS' | 'S' | 'T' | 'Ag' | 'Int' | 'WP' | 'Fel';
export type SecondaryCharacteristics = 'A' | 'W' | 'SB' | 'TB' | 'M' | 'Mag' | 'IP' | 'FP';

export interface ICharacteristic {
  type: string;
  name: string;
  starting: number;
  advanced: number;
  current: number;
}

export interface ISkill {
  name: string;
  taken: boolean;
  total: number;
  relatedCharacteristic: MainCharacteristics;
  relatedTalent?: string;
}

export interface IInventory {
  trappings: ITrapings[];
  Weapons: IWeapon[];
  Armor: IArmor[];
  moneyAndTreasure: IGoldAndTreasure;
}

export interface ICharacter {
  name: string;
  race: string;
  careerPath: string[];
  totalExp: number;
  spendExp: number;
  gender: string;
  birthDate: string;
  nationality: string;
  height: string;
  eyes: string;
  age: string;
  birthplace: string;
  religion: string;
  weight: string;
  hair: string;
  distinguishingMark: string;
  characteristics: ICharacteristic[];
  talents: ITalent[];
  Skills: ISkill[];
  Inventory: IInventory;
  movement: IMovement;
  spells: ISpell[];
}

