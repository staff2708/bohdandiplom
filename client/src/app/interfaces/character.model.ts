import { ICharacter } from './character.interface';

export class CharacterModel {
  constructor(
    {
      Inventory,
      Skills,
      age,
      birthDate,
      birthplace,
      careerPath,
      characteristics,
      distinguishingMark,
      eyes,
      gender,
      hair,
      height,
      movement,
      name,
      nationality,
      race,
      religion,
      spells,
      spendExp,
      talents,
      totalExp,
      weight,
    }: ICharacter) {}
}
