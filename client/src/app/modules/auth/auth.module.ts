import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './components/login/login.component';
import { RegistrationComponent } from './components/registration/registration.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { AuthRouting } from './auth.routing';
import { AuthComponent } from './components/auth/auth.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [LoginComponent, RegistrationComponent, ForgotPasswordComponent, AuthComponent],
  imports: [
    CommonModule,
    AuthRouting,
    ReactiveFormsModule
  ]
})
export class AuthModule { }
