import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../../../services/auth.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {

  @ViewChild('confirmPassword') confPass: ElementRef;

  public form = new FormGroup({
    email: new FormControl('', [
      Validators.required,
      Validators.minLength(6),
    ]),
    nickname: new FormControl('', [
      Validators.required,
      Validators.minLength(6),
    ]),
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(6),
    ]),
  });

  constructor(
    private auth: AuthService,
  ) { }

  ngOnInit() {
  }

  public onSubmit(): void {
    if( this.form.valid && this.form.get('password').value ===  this.confPass.nativeElement.value) {
      this.auth.registration(this.form.value).subscribe();
    }
  }

}
