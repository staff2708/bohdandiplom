import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { IUser } from '../interfaces/IUser';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private url = 'http://localhost:5000/api/auth';
  private jwtToken: string = '';


  constructor(
    private http: HttpClient,
  ) {
  }

  public registration( body: IUser ): Observable<any> {
    return this.http.post<IUser>(`${this.url}/registration/`, {
      email: body.email,
      password: body.password,
      nickname: body.nickname
    }, {
      observe: 'response'

    })
  }

  public getJwtToken(): string {
    return this.jwtToken;
  }

  public login( body: IUser ): Observable<any> {
    return this.http.post<IUser>(`${this.url}/login`, { email: body.email, password: body.password }, {
      observe: 'response'
    }).pipe(tap((res) => this.jwtToken = res.headers.get('JWTAuthorization') || ''))
  }
}
