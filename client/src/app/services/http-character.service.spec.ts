import { TestBed } from '@angular/core/testing';

import { HttpCharacterService } from './http-character.service';

describe('HttpCharacterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HttpCharacterService = TestBed.get(HttpCharacterService);
    expect(service).toBeTruthy();
  });
});
