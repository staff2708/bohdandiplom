import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ICharacter } from '../interfaces/character.interface';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class HttpCharacterService {

  private url = 'http://localhost:5000/api/character';

  constructor(
    private http: HttpClient,
    private auth: AuthService
    ) {}

  public getAllHeros(): Observable<ICharacter[]> {
    return this.http.get<ICharacter[]>(this.url);
  }

  public getHeroById(id: string): Observable<ICharacter> {
    return this.http.get<ICharacter>(this.url + id);
  }

  public createHero(hero: ICharacter): Observable<ICharacter> {

    const httpOptions = new HttpHeaders({'JWTAuthorization': this.auth.getJwtToken()});
    return this.http.post<ICharacter>(this.url, hero, { headers: httpOptions });
  }

  public deleteHero(id: number): Observable<ICharacter> {
    return this.http.delete<ICharacter>(this.url + id);
  }

  public updateHero(id: number, hero: ICharacter): Observable<ICharacter> {
    return this.http.put<ICharacter>(this.url + id, hero);
  }

}
