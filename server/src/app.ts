import * as express from 'express';
import * as mongoose from 'mongoose';
import { IController } from './interfaces/controller.interface';
import errorMiddleware from './middlewares/error.middleware';
// @ts-ignore
import * as cors from 'cors';
import { RedisController } from './controllers/redis.controller';

class App {
    public app: express.Application;
    public port: number;

    constructor(controllers: IController[], port: number) {
        this.app = express();
        this.port = port;
        this.connectToTheDatabase();
        this.initializeMiddlewares();
        this.initializeControllers(controllers);
        this.initializeErrorHandling();
    }

    public listen() {
        this.app.listen(this.port, () => {
            console.log(`App listening on the port ${this.port}`);
        });
    }

    private initializeMiddlewares() {
        this.app.use(express.json());
        this.app.use(this.allowCors);
        this.app.use(cors());
    }

    private initializeControllers(controllers: any) {
        controllers.forEach((controller: any) => {
            this.app.use('/api', controller.router);
        });
    }

    private connectToTheDatabase(): void {
        const MONGO_PATH = 'localhost:27017';
        mongoose.connect(`mongodb://${MONGO_PATH}`);
        RedisController.getInstance();
    }

    private allowCors(req: express.Request, res: express.Response, next: any) {
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Access-Control-Allow-Methods', '*');
        res.setHeader('Access-Control-Allow-Headers', '*');
        res.setHeader('Access-Control-Expose-Headers', '*');
        next();
    }

    private initializeErrorHandling() {
        this.app.use(errorMiddleware);
    }

}

export default App;
