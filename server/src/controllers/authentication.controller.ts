import * as bcrypt from 'bcrypt';
import * as express from 'express';
import { IController } from '../interfaces/controller.interface';
import { userModel } from '../models/user.model';
import UserWithThatEmailAlreadyExistsException from '../exceptions/UserWithThatEmailAlreadyExistsException';
import WrongCredentialsException from '../exceptions/WrongCredentialsException';
import { IDataStoredInToken, ITokenData } from '../interfaces/tokenData.interface';
import { IUser } from '../interfaces/user.interface';

import * as jwt from 'jsonwebtoken';
import { RedisController } from './redis.controller';
import authMiddleware from '../middlewares/auth.middleware';

class AuthenticationController implements IController {
    public path = '/auth';
    public router = express.Router();
    private user = userModel;
    private redis: RedisController;

    constructor() {
        this.initializeRoutes();
        this.redis = RedisController.getInstance();
    }

    private initializeRoutes() {
        this.router.post(`${this.path}/registration`, this.registration);
        this.router.post(`${this.path}/login`, this.loggingIn);
        this.router.post(`${this.path}/logout`, this.loggingOut);
        this.router.post(`$${this.path}/check`, authMiddleware, this.checkAuth)
    }

    private registration = async (request: express.Request, response: express.Response, next: express.NextFunction) => {
        const userData = request.body;
        if (
            await this.user.findOne({ email: userData.email })
        ) {
            next(new UserWithThatEmailAlreadyExistsException(userData.email));
        } else {
            const hashedPassword = await bcrypt.hash(userData.password, 10);
            const user = await this.user.create({
                ...userData,
                password: hashedPassword,
            });
            user.password = undefined;
            const tokenData = this.createToken(user);
            response.setHeader('JWTAuthorization', tokenData.token);
            response.send(user);
        }
    };

    private loggingIn = async (request: express.Request, response: express.Response, next: express.NextFunction) => {
        const logInData = request.body;
        const user = await this.user.findOne({ email: logInData.email });
        if (user) {
            const isPasswordMatching = await bcrypt.compare(logInData.password, user.password);
            if (isPasswordMatching) {
                user.password = undefined;
                const tokenData = this.createToken(user);
                this.redis.setToken(user._id, tokenData.token, () => {
                    response.setHeader('JWTAuthorization', tokenData.token);
                    response.send(user);
                });
            } else {
                next(new WrongCredentialsException());
            }
        } else {
            next(new WrongCredentialsException());
        }
    };

    private checkAuth = async (request: express.Request, response: express.Response, next: express.NextFunction) => {
        response.status(200).send();
    };

    private createToken(user: IUser): ITokenData {
        const expiresIn = 60 * 60; // an hour
        const secret = process.env.JWT_SECRET;
        const dataStoredInToken: IDataStoredInToken = {
            _id: user._id,
        };
        return {
            expiresIn,
            token: jwt.sign(dataStoredInToken, secret, { expiresIn }),
        };
    };

    private loggingOut = (request: express.Request, response: express.Response) => {
        response.send(200);
    }
}

export default AuthenticationController;
