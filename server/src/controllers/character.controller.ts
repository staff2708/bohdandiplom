import * as express from 'express';
import characterModel, { ICharacter } from '../models/character.model';
import { IController } from '../interfaces/controller.interface';
import { NextFunction } from 'express';
import CharacterNotFoundException from '../exceptions/CharacterNotFoundException';
import authMiddleware from '../middlewares/auth.middleware';

class CharacterController implements IController {
    public path = '/character';
    public router = express.Router();

    constructor() {
        this.initializeRoutes();
    }

    private initializeRoutes() {
        this.router.use(this.path, authMiddleware);
        this.router.get(`${this.path}`, this.getAllCharacters);
        this.router.get(`${this.path}/:id`, this.getCharacterById);
        this.router.delete(`${this.path}/:id`, this.deleteCharacter);
        this.router.post(`${this.path}`, this.createCharacter);
        this.router.put(`${this.path}`, this.updateCharacter);
    }

    private getAllCharacters = ( request: express.Request, response: express.Response, next: NextFunction ): void => {
        characterModel.find().then(( characters: ICharacter[] ) => {
            response.send(characters);
        });
    };

    private getCharacterById = ( request: express.Request, response: express.Response, next: NextFunction ): void => {
        const id = request.params.id;
        characterModel.findById(id).then(( character: ICharacter ) => {
            if ( character ) {
                response.send(character);
            } else {
                next(new CharacterNotFoundException(id));
            }
        });
    };
    private createCharacter = ( request: express.Request, response: express.Response, next: NextFunction ): void => {
        const character: ICharacter = request.body;
        const createdCharacter = new characterModel(character);
        createdCharacter.save().then(( data: ICharacter ) => {
            response.send(data);
        });
    };
    private updateCharacter = ( request: express.Request, response: express.Response, next: NextFunction ): void => {
        const id = request.params.id;
        const characterData: ICharacter = request.body;
        characterModel.findByIdAndUpdate(id, characterData).then(( updatedCharacter: ICharacter ) => {
            if (updatedCharacter) {
                response.send(updatedCharacter);
            } else {
                next(new CharacterNotFoundException(id));
            }
        });
    };
    private deleteCharacter = ( request: express.Request, response: express.Response, next: NextFunction ): void => {
        const id = request.params.id;
        characterModel.findByIdAndDelete(id).then(( res: any ) => {
            if ( res ) {
                response.send(200);
            } else {
                next(new CharacterNotFoundException(id));
            }
        });
    }

}

// @ts-ignore
export default CharacterController;
