import * as redis from 'redis';
import { RedisClient } from 'redis';
import AuthenticationTokenMissingException from '../exceptions/AuthenticationTokenMissingException';
import WrongAuthenticationTokenException from '../exceptions/WrongAuthenticationTokenException';

export class RedisController {
    private static instance: RedisController;
    private redis: RedisClient;

    private constructor(){
        this.redis = redis.createClient();
    }

    public static getInstance(): RedisController {
        if (!RedisController.instance) {
            RedisController.instance = new RedisController();
        }

        return RedisController.instance;
    }

    public setToken(id: string, token: string, cb: Function) {
        this.redis.set(id, token, () => {
            cb();
            this.redis.expire(id, 3600);
        })
    }

    public async getToken(id: string, cb?: Function) {
        await this.redis.exists(id, async (err, reply) => {
            if(reply) {
                return await this.redis.get(id, () => {
                    if(cb) {
                        cb();
                    }
                });
            } else {
                throw new WrongAuthenticationTokenException();
            }
        });
    }
}
