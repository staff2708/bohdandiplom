import * as express from 'express';
import { IController } from '../interfaces/controller.interface';
import { userModel } from '../models/user.model';
import validationMiddleware from '../middlewares/validation.middleware';
import CreateUserDTO from '../DTO/user.dto';
import { IUser } from '../interfaces/user.interface';

class UserController implements IController {
    public path = '/character';
    public router = express.Router();

    constructor() {
        this.initializeRoutes();
    }

    private initializeRoutes() {
        this.router.get(`${this.path}`, this.getAllUsers);
        this.router.get(`${this.path}/:id`, this.getUserById);
        this.router.delete(`${this.path}/:id`, this.deleteUser);
        this.router.post(`${this.path}`, validationMiddleware(CreateUserDTO),this.createUser);
        this.router.put(`${this.path}`, this.updateUser);
    }

    private getAllUsers = (request: express.Request, response: express.Response): void => {
        userModel.find().then((user: IUser[]) => {
            response.send(user);
        });
    };

    private getUserById = (request: express.Request, response: express.Response): void => {
        const id = request.params.id;
        userModel.findById(id).then((user: IUser) => {
            response.send(user);
        });
    };
    private createUser = (request: express.Request, response: express.Response): void => {
        const user: IUser = request.body;
        const createdUser = new userModel(user);
        createdUser.save().then((data: IUser) => {
            response.send(data);
        });
    };
    private updateUser = (request: express.Request, response: express.Response): void => {
        const id = request.params.id;
        const userData: IUser = request.body;
        userModel.findByIdAndUpdate(id, userData).then((updatedUser: IUser) => {
            response.send(updatedUser);
        });
    };
    private deleteUser = (request: express.Request, response: express.Response): void => {
        const id = request.params.id;
        userModel.findByIdAndDelete(id).then((res: any) => {
            if (res) {
                response.send(200);
            } else {
                response.send(404);
            }
        });
    }

}

// @ts-ignore
export default UserController;
