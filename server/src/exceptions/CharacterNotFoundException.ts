import HttpException from "./HttpException";

class CharacterNotFoundException extends HttpException {
    constructor(id: string) {
        super(404, `Character with id ${id} not found`);
    }
}

export default CharacterNotFoundException;
