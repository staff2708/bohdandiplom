import { ICharacter } from '../models/character.model';
import { Request } from 'express';

export interface IUser {
    _id: string;
    email: string,
    nickname: string,
    password?: string,
    token?: string,
    characters?: ICharacter[],
    groups?: any[],
    image?: any
}

export interface RequestWithUser extends Request {
    user: IUser;
}
