import { NextFunction, Response } from 'express';
import * as jwt from 'jsonwebtoken';
import { userModel } from '../models/user.model';
import { IDataStoredInToken } from '../interfaces/tokenData.interface';
import { RequestWithUser } from '../interfaces/user.interface';
import WrongAuthenticationTokenException from '../exceptions/WrongAuthenticationTokenException';
import AuthenticationTokenMissingException from '../exceptions/AuthenticationTokenMissingException';
import { RedisController } from '../controllers/redis.controller';

async function authMiddleware(request: RequestWithUser, response: Response, next: NextFunction) {
    const headers = request.headers;
    if (headers && headers.jwtauthorization) {
        const secret = process.env.JWT_SECRET;
        try {
            const verificationResponse = jwt.verify((headers.jwtauthorization as string), secret) as IDataStoredInToken;
            const id = verificationResponse._id;
            const isValid = !!RedisController.getInstance().getToken(id);
            const user = await userModel.findById(id);
            if (user && isValid) {
                request.user = user;
                next();
            } else {
                next(new WrongAuthenticationTokenException());
            }
        } catch (error) {
            next(new WrongAuthenticationTokenException());
        }
    } else {
        next(new AuthenticationTokenMissingException());
    }
}

export default authMiddleware;
