import * as mongoose from 'mongoose';
import { characteristicSchema } from './characteristic.model';
import { inventorySchema } from './inventory.model';
import { movementSchema } from './movement.model';
import { skilSchema } from './skill.model';
import { spellSchema } from './spell.model';
import { talentSchema } from './talents.model';
import { IArmor } from '../../../client/src/app/interfaces/character.interface';

interface ITrapings {
    item: string;
    location: string;
    Enc: number;
}

interface IWeapon {
    name: string;
    group: string;
    damage: string;
    range: string;
    reload: string;
    qualities: string[];
}

interface IArmour {
    armorType: string;
    locationCovered: string;
    Enc: number;
    AP: number;
}

interface IGoldAndTreasure {
    gold: number;
    silver: number;
    brass: number;
    treasures: string[];
}

interface IMovement {
    M: number;
    armouredM: number;
    move: number;
    charge: number;
    run: number;
    runningLeap: number;
    standingLeap: number;
    flying: number;
    hamperedMovement: IMovementAbstraction;
    standartMovement: IMovementAbstraction;
}

interface ISpell {
    name: string;
    castingNumber: number;
    castingTime: string;
    ingredients: string;
    descriptions: string;
}

interface IMovementAbstraction {
    yardsPerMinute: number;
    milesPerMinute: number;
}

interface ITalent {
    name: string;
    description: string;
}

type MainCharacteristics = 'WS' | 'BS' | 'S' | 'T' | 'Ag' | 'Int' | 'WP' | 'Fel';
type SecondaryCharacteristics = 'A' | 'W' | 'SB' | 'TB' | 'M' | 'Mag' | 'IP' | 'FP';

interface ICharacteristic {
    type: MainCharacteristics | SecondaryCharacteristics;
    name: string;
    starting: number;
    advanced: number;
    current: number;
}

interface ISkill {
    name: string;
    taken: boolean;
    total: number;
    relatedCharacteristic: ICharacteristic;
    relatedTalent?: ITalent;
}

interface IInventory {
    trappings: ITrapings[];
    Weapons: IWeapon[];
    Armor: IArmor[];
    moneyAndTreasure: IGoldAndTreasure;
}

export interface ICharacter {
    name: string;
    race: string;
    careerPath: string;
    totalExp: number;
    spendExp: number;
    gender: string;
    birthDate: string;
    nationality: string;
    height: string;
    eyes: string;
    age: string;
    birthplace: string;
    religion: string;
    weight: string;
    hair: string;
    maxEnc: number,
    totalEnc: number,
    distinguishingMark: string;
    mainCharacteristics: ICharacteristic[];
    secondaryCharacteristics: ICharacteristic[];
    talents: ITalent[];
    skills: ISkill[];
    inventory: IInventory;
    movement: IMovement;
    spells: ISpell[];
}

export const characterSchema = new mongoose.Schema({
        name: String,
        race: String,
        careerPath: String,
        totalExp: Number,
        spendExp: Number,
        gender: String,
        birthDate: String,
        nationality: String,
        height: String,
        eyes: String,
        age: String,
        birthplace: String,
        religion: String,
        weight: String,
        hair: String,
        distinguishingMark: String,
        maxEnc: Number,
        totalEnc: Number,
        mainCharacteristics: [characteristicSchema],
        secondaryCharacteristics: [characteristicSchema],
        talents: [talentSchema],
        skills: [skilSchema],
        inventory: inventorySchema,
        movement: movementSchema,
        spells: [spellSchema],
});

// @ts-ignore
const characterModel = mongoose.model<ICharacter & mongoose.Document>('Character', characterSchema);

export default characterModel;
