import * as mongoose from 'mongoose';

export const characteristicSchema = new mongoose.Schema({
    type: String,
    name: String,
    starting: Number,
    advanced: Number,
    current: Number,
});
