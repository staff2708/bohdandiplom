import * as mongoose from 'mongoose';

export const armorSchema = new mongoose.Schema({
    armorType: String,
    locationCovered: String,
    Enc: Number,
    AP: Number,
});

export const weaponSchema = new mongoose.Schema({
    name: String,
    group: String,
    damage: String,
    range: String,
    reload: String,
    qualities: String,
});

export const trappingSchema = new mongoose.Schema({
    item: String,
    location: String,
    Enc: Number,
});

export const moneyAndTreasure = new mongoose.Schema({
    gold: Number,
    silver: Number,
    brass: Number,
    treasures: String,
});

export const inventorySchema = new mongoose.Schema({
    trappings: [trappingSchema],
    Weapons: [weaponSchema],
    Armor: [armorSchema],
    moneyAndTreasure: [moneyAndTreasure],
});
