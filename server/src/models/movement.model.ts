import * as mongoose from 'mongoose';

const movementAbstractionSchema = new mongoose.Schema({
    yardsPerMinute: Number,
    milesPerMinute: Number,
});

export const movementSchema = new mongoose.Schema({
    M: Number,
    armouredM: Number,
    move: Number,
    charge: Number,
    run: Number,
    runningLeap: Number,
    standingLeap: Number,
    flying: Number,
    hamperedMovement: movementAbstractionSchema,
    standartMovement: movementAbstractionSchema,
});
