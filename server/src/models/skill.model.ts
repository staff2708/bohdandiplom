import * as mongoose from 'mongoose';

export const skilSchema = new mongoose.Schema({
    name: String,
    taken: Boolean,
    total: Number,
    relatedCharacteristic: String,
    relatedTalent: String,
});
