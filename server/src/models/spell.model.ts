import * as mongoose from 'mongoose';

export const spellSchema = new mongoose.Schema({
    name: String,
    castingNumber: Number,
    castingTime: String,
    ingredients: String,
    descriptions: String,
});
