import * as mongoose from 'mongoose';

export const talentSchema = new mongoose.Schema({
    name: String,
    description: String,
});
