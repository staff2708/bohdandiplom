import { characterSchema } from './character.model';
import * as mongoose from 'mongoose';
import { IUser } from '../interfaces/user.interface';

export const userSchema = new mongoose.Schema({
    email: String,
    nickname: String,
    password: String,
    characters: [characterSchema],
    groups: [],
    image: {
        data: Buffer,
        contentType: String,
    }
});

export const userModel = mongoose.model<IUser & mongoose.Document>('User', userSchema);
