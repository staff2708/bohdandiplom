import App from './app';
import CharacterController from './controllers/character.controller';
import 'dotenv/config';
import AuthenticationController from './controllers/authentication.controller';

const app = new App(
    [
        new CharacterController(),
        new AuthenticationController(),
    ],
    5000,
);

app.listen();
